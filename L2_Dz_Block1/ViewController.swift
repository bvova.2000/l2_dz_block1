//variant 1.2
//  ViewController.swift
//  Dz_L1_1
//
//  Created by Hen Joy on 4/8/19.
//  Copyright Â© 2019 Hen Joy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        WorkWithFunction()
        
    }
    
    func WorkWithFunction(){
        let firstDig = Int.random(in: 1..<100)
        var secondDigit = Int.random(in: 1..<100)
        var result = 0
        result = task1ShowBigestDigit(firstDig, and: secondDigit)
        print("Biggest digit is first digit = \(result)")
        task2ShowSquareAndCubeDigit(firstDig)
        task3ShowAllDigitTill(secondDigit)
        task4ShowDividersOfDigit(firstDig)
        secondDigit = Int.random(in: 1...10000)
        let res = task5FindPerfectDigit(secondDigit)
        if res{
            print("Perfect digit is \(secondDigit)")
        }else{
            print("Digit \(secondDigit) is not Perfect")
        }
    }
    
    func task1ShowBigestDigit (_ numA: Int, and numB: Int) -> Int {
        if numA > numB{
            return numA
        }else{
            return numB
        }
    }
    
    func task2ShowSquareAndCubeDigit(_ dig: Int){
        let resultSquare = dig * dig
        let resultCube = dig * dig * dig
        print("Squre of digit = \(resultSquare)")
        print("Squre of digit = \(resultCube)")
    }
    
    func task3ShowAllDigitTill(_ dig: Int){
        var counter = 0
        print("Digit to up: ")
        for _ in 0...dig{
            print(counter)
            counter += 1
        }
        print("Digit to down: ")
        for _ in 0...dig{
            counter -= 1
            print(counter)
        }
    }
    
    func task4ShowDividersOfDigit(_ dig: Int){
        var resultSum = 0
        for i in 1...dig - 1 where dig % i == 0{
            resultSum += 1
        }
        print("Count of \(dig) dividers = \(resultSum)")
    }
    
    func task5FindPerfectDigit(_ dig: Int) -> Bool{
        var resultSum = 0
        for i in 1...dig - 1 where dig % i == 0{
            resultSum += i
        }
        if resultSum == dig{
            return true
        }else{
            return false
        }
        
    }
}
